// hooks
import useLessons from '../context/useLessons'

// components
import Table from '../components/table/Table'

export default function AreaPage({ currArea }) {

  const { formattedLessons } = useLessons()

  return (
    <div>
      <h3>{currArea.name}</h3>
      <Table tableArr={formattedLessons}/>
    </div>
  )
}
