// hooks
import useArea from '../context/useArea'

// containers
import { LessonsProvider } from '../context/LessonsProvider'

// components
import AreaSelector from '../components/AreaSelector'
import AreaPage from '../pages/AreaPage'

export default function Main() {

  const { areas, currArea, setAreaId } = useArea()

  return (
    <div>
      {!currArea && <AreaSelector areas={areas} setAreaId={setAreaId} />}
      {currArea && (
        <LessonsProvider areaId={currArea.id}>
          <AreaPage currArea={currArea} />
        </LessonsProvider>
      )}
    </div>
  )
}
