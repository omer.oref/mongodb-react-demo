export default function AreaSelector({ areas, setAreaId }) {
  return (
    <div style={{ display: 'flex', justifyContent: 'center'}}>
      {areas.map(area => (
        <button
          key={area.id}
          onClick={() => setAreaId(area.id)}
        >{area.name}</button>
      ))}
    </div>
  )
}
