// components
import TableHead from './TableHead'
import TableBody from './TableBody'

export default function Table({ tableArr }) {

  return (
    <>
      <table className="table">
        <TableHead />
        <TableBody
          tableArr={tableArr}
        />
      </table>
    </>
  )
}