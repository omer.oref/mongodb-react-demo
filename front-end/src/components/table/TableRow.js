export default function TableRow({ row }) {

  const {
    id,
    drill,
    debrief,
    subject,
    subjectPlus,
    stone,
    mission,
    date,
    status,
    statusPlus,
    responsibility
  } = row

  return (
    <tr>
      <td>{id}</td>
      <td>{drill}</td>
      <td>{debrief}</td>
      <td>{subject}</td>
      <td>{subjectPlus}</td>
      <td>{stone}</td>
      <td>{mission}</td>
      <td>{date}</td>
      <td>{status}</td>
      <td>{statusPlus}</td>
      <td>{responsibility}</td>
    </tr>
  )
}
