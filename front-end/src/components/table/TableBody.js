import TableRow from './TableRow'

export default function TableBody({ tableArr }) {
  return (
    <tbody>
      {tableArr.map(row =>
        <TableRow 
          key={row.id} 
          row={row} 
        />
      )}
    </tbody>
  )
}