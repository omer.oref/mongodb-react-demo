import { useEffect, useState } from "react"

export default url => {

  const [data, setData] = useState(null)
  const [isLoading, setIsLoading] = useState(false)
  const [error, setError] = useState(null)

  useEffect(() => {
    if (url) {
      (async () => {
        setIsLoading(true)
        try {
          setIsLoading(false)
          const res = await fetch(url)
          const resData = await res.json()
          setData(resData)
        } catch (err) {
          setIsLoading(false)
          console.log(err);
          setError(`${err}`)
        }
      })()
    } else {
      setData(null)
      setIsLoading(false)
      setError('No url Defined')
    }
  }, [url])
  
  return { data, isLoading, error }
}