// containers
import { AreaProvider } from './context/AreaProvider'

// components
import Main from './pages/Main'

export default function App() {

  return (
    <div className="App">
      <AreaProvider>
        <Main />
      </AreaProvider>
    </div>
  )
}