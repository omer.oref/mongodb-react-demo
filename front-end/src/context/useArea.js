import { useContext } from "react"
import { AreaContext } from "./AreaProvider"

export default function useArea() {
  const context = useContext(AreaContext)

  if (context == undefined || (context.length != undefined && context.length == 0)) {
    throw new Error('useAreas() must be used inside a AreasProvider')
  }

  return context
}