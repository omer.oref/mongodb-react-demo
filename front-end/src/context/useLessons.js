import { useContext } from "react"
import { LessonsContext } from "./LessonsProvider"

export default function useLessons() {
  const context = useContext(LessonsContext)

  if (context == undefined || (context.length != undefined && context.length == 0)) {
    throw new Error('useLessons() must be used inside a LessonsProvider')
  }

  return context
}