import { createContext } from 'react'

// hooks
import useFetch from '../hooks/useFetch'
import useFormatArr from '../hooks/useFormatArr'

// context
export const LessonsContext = createContext()

export function LessonsProvider({ children, areaId }) {
  
  const { data: lessons, isLoading, error } = useFetch(`/areas/${areaId}/lessons`)
  const formattedLessons = useFormatArr(lessons)

  return (
    <LessonsContext.Provider value={{ lessons, formattedLessons }}>
        {/* pending */}
        {isLoading && <h3>Loading...</h3>}
        {/* error */}
        {error && <h3>{error}</h3>}
        {/* loaded */}
        {!error && lessons && children}
    </LessonsContext.Provider>
  )
}
