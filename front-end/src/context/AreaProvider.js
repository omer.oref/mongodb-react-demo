import { createContext, useState, useEffect } from 'react'

// hooks
import useFetch from '../hooks/useFetch'

// context
export const AreaContext = createContext()

export function AreaProvider({ children }) {
  
  const [areaId, setAreaId] = useState(null)
  const [currArea, setCurrArea] = useState(null)

  const { data: areas, isLoading, error } = useFetch('/areas')

  useEffect(() => {
    if (areas && areaId) {
      setCurrArea(areas.find(area => area.id == areaId))
    } else {
      setCurrArea(null)
    }
  }, [areaId, areas])

  return (
    <AreaContext.Provider value={{ areas, setAreaId, currArea }}>
        {/* pending */}
        {isLoading && <h3>Loading...</h3>}
        {/* error */}
        {error && <h3>{error}</h3>}
        {/* loaded */}
        {!error && areas && children}
    </AreaContext.Provider>
  )
}
