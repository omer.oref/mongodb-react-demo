const express = require('express')
const { v4: uuidv4 } = require('uuid');

let areas = [
  {
    id: "123",
    name: 'מחלקת רמ"ט',
    lessonsCreated: 2,
  }
]

let lessons = {
  "123": [
    {
      "id": 1,
      "drill": {
          "name": "מבצע קרן אור 2020",
          "type": "מבצע",
          "date": "20-10-2020"
      },
      "debrief": "הסברה ותקשורת",
      "subject": "כללי",
      "subjectPlus": " תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.",
      "stone": "ציר פיקוד אורכי",
      "mission": "קראס אגת לקטוס וואל אאוגו",
      "date": {
          "name": "30-5-2021",
          "year": 2021
      },
      "status": "בביצוע",
      "statusPlus": "לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור",
      "responsibility": [
          "רע\"ן אגמים",
          "רע\"ן תוהד",
          "רמ\"ח אוכ"
      ]
    },
    {
      "id": 2,
      "drill": {
          "name": "מבצע קרן אור 2020",
          "type": "מבצע",
          "date": "20-10-2020"
      },
      "debrief": "הסברה ותקשורת",
      "subject": "כללי",
      "subjectPlus": " תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.",
      "stone": "ציר פיקוד אורכי",
      "mission": "קראס אגת לקטוס וואל אאוגו",
      "date": {
          "name": "30-5-2021",
          "year": 2021
      },
      "status": "בביצוע",
      "statusPlus": "לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור",
      "responsibility": [
          "רמ\"ח אוכ"
      ]
    },
  ]
}

// init app
const app = express()
app.use(express.json())


// areas
app.get('/api/areas', (req, res) => {
  res.send(areas)
})

app.get('/api/areas/:areaId', (req, res) => {
  
  const { areaId } = req.params
  const area = areas.find(area => area.id == areaId)

  if (area) {
    res.status(200).send(area)
  } else {
    res.status(404).send('Area Not Found!')
  }
})

app.post('/api/areas', (req, res) => {

  const area = {
    id: uuidv4(),
    lessonsCreated: 0,
    ...req.body
  }

  areas.push(area)
  res.status(200).send(areas)
})

app.delete('/api/areas/:areaId', (req, res) => {
  
  const { areaId } = req.params
  areas = areas.filter(area => area.id != areaId)

  res.status(200).send(areas)
})


// lessons
app.get('/api/areas/:areaId/lessons', (req, res) => {
  
  const { areaId } = req.params
  const area = areas.find(area => area.id == areaId)
  
  if (area && lessons[areaId]) {
    res.status(200).send(lessons[areaId])
  } else {
    res.status(404).send('Area Lessons Not Found!')
  }
})

app.get('/api/areas/:areaId/lessons/:lessonId', (req, res) => {
  
  const { areaId, lessonId } = req.params
  const area = areas.find(area => area.id == areaId)

  if (area && lessons[areaId]) {
    
    const lesson = lessons[areaId].find(lesson => lesson.id == lessonId)
  
    if (lesson) {
      res.status(200).send(lesson)
    } else {
      res.status(404).send('Lesson Not Found!')
    }

  } else {
    res.status(404).send('Area Lessons Not Found!')
  }
})

app.post('/api/areas/:areaId/lessons', (req, res) => {

  const { areaId } = req.params
  const area = areas.find(area => area.id == areaId)

  if (area && lessons[areaId]) {
    
    area.lessonsCreated++

    const lesson = {
      id: area.lessonsCreated,
      ...req.body
    }

    lessons[areaId].push(lesson)
    res.status(200).send(lessons[areaId])

  } else {
    res.status(404).send('Area Lessons Not Found!')
  }
})

app.delete('/api/areas/:areaId/lessons/:lessonId', (req, res) => {

  const { areaId, lessonId } = req.params
  const area = areas.find(area => area.id == areaId)
  if (area) {

    lessons[areaId] = lessons[areaId].filter(lesson => lesson.id != lessonId)
    res.status(200).send(lessons[areaId])
    
  } else {
    res.status(404).send('Area Lessons Not Found!')
  }
})


// deploy
app.listen(8000, () => console.log('listening on port 8000'))